import { apiErrorHandler } from './api/apiErrorHandler';
import express from 'express';
import { initRestApi } from './api/api';

const app = express();

app.listen(8090, () => {
  console.log('Server is now running on port 8090...');
});

initRestApi(app);

app.use(apiErrorHandler);
