"use strict";
exports.__esModule = true;
var apiErrorHandler_1 = require("./api/apiErrorHandler");
var express_1 = require("express");
var api_1 = require("./api/api");
var app = (0, express_1["default"])();
app.listen(8090, function () {
    console.log('Server is now running on port 8090...');
});
(0, api_1.initRestApi)(app);
app.use(apiErrorHandler_1.apiErrorHandler);
