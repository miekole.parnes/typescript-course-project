import { Sequelize, Model, DataTypes } from '@sequelize/core';

export function initLessonModel(seq: Sequelize) {
  class LessonsModel extends Model {}
  return LessonsModel.init(
    {
      url: { type: DataTypes.STRING },
      description: { type: DataTypes.STRING },
      duration: { type: DataTypes.STRING },
      seqNo: { type: DataTypes.INTEGER },
      courseId: { type: DataTypes.INTEGER },
      pro: { type: DataTypes.BOOLEAN },
      tags: { type: DataTypes.STRING },
      gitHubUrl: { type: DataTypes.STRING },
    },
    {
      modelName: 'Lessons',
      sequelize: seq,
    }
  );
}
