"use strict";
exports.__esModule = true;
exports.createLesson = void 0;
var model_1 = require("../model/model");
function createLesson(props) {
    return model_1.LessonModel.create(props);
}
exports.createLesson = createLesson;
