import { Request, Response } from 'express';
import { createLesson } from '../data/queries/createLesson';
import { onError } from './onError';
import { onSuccess } from './onSuccess';
import * as _ from 'lodash';

export function apiCreateLesson(req: Request, res: Response) {
  createLesson(req.body)
    .then(_.partial(onSuccess, res))
    .catch(err => {
      console.error(err);
    })
    .catch(_.partial(onError, res, 'Could not create lesson'));
}
