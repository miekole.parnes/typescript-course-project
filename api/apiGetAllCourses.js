"use strict";
exports.__esModule = true;
exports.apiGetAllCourses = void 0;
var _ = require("lodash");
var findAllCourses_1 = require("../data/queries/findAllCourses");
var onError_1 = require("./onError");
var onSuccess_1 = require("./onSuccess");
function apiGetAllCourses(req, res) {
    (0, findAllCourses_1.findAllCourses)()
        .then(_.partial(onSuccess_1.onSuccess, res))["catch"](_.partial(onError_1.onError, res, 'Find All Courses Failed'));
}
exports.apiGetAllCourses = apiGetAllCourses;
