"use strict";
exports.__esModule = true;
exports.apiCreateLesson = void 0;
var createLesson_1 = require("../data/queries/createLesson");
var onError_1 = require("./onError");
var onSuccess_1 = require("./onSuccess");
var _ = require("lodash");
function apiCreateLesson(req, res) {
    (0, createLesson_1.createLesson)(req.body)
        .then(_.partial(onSuccess_1.onSuccess, res))["catch"](function (err) {
        console.error(err);
    })["catch"](_.partial(onError_1.onError, res, 'Could not create lesson'));
}
exports.apiCreateLesson = apiCreateLesson;
