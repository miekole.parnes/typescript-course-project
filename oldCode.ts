// const { Sequelize, Model, DataTypes } = require('@sequelize/core');
// const dbUrl =
//   'postgres://postgres:postgres@localhost:5432/complete-typescript-course';
// const sequelize = new Sequelize(dbUrl);
// console.log(tryModel);
// class CourseModel extends Model {}
// CourseModel.init(
//   {
//     description: { type: DataTypes.STRING },
//     url: { type: DataTypes.STRING },
//     longDescription: { type: DataTypes.TEXT },
//     iconUrl: { type: DataTypes.STRING },
//     courseListIcon: { type: DataTypes.STRING },
//     seqNo: { type: DataTypes.INTEGER },
//     comingSoon: { type: DataTypes.BOOLEAN },
//     isNew: { type: DataTypes.BOOLEAN },
//     isOngoing: { type: DataTypes.BOOLEAN },
//   },
//   {
//     tableName: 'Courses',
//     sequelize: sequelize,
//   }
// );

// async function courseM() {
//   const model1 = await CourseModel().findAll({
//     order: ['seqNo'],
//   });
//   console.log(JSON.parse(JSON.stringify(model1)));
// }

// courseM();

// app.route('/api/courses').get((req, res) => {
//     CourseModel()
//       .findAll({
//         order: ['seqNo'],
//       })
//       .then(result => {
//         res.status(200).json({ result });
//       });
//   });
