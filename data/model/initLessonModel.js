"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
exports.initLessonModel = void 0;
var core_1 = require("@sequelize/core");
function initLessonModel(seq) {
    var LessonsModel = /** @class */ (function (_super) {
        __extends(LessonsModel, _super);
        function LessonsModel() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return LessonsModel;
    }(core_1.Model));
    return LessonsModel.init({
        url: { type: core_1.DataTypes.STRING },
        description: { type: core_1.DataTypes.STRING },
        duration: { type: core_1.DataTypes.STRING },
        seqNo: { type: core_1.DataTypes.INTEGER },
        courseId: { type: core_1.DataTypes.INTEGER },
        pro: { type: core_1.DataTypes.BOOLEAN },
        tags: { type: core_1.DataTypes.STRING },
        gitHubUrl: { type: core_1.DataTypes.STRING }
    }, {
        modelName: 'Lessons',
        sequelize: seq
    });
}
exports.initLessonModel = initLessonModel;
