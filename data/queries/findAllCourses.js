"use strict";
exports.__esModule = true;
exports.findAllCourses = void 0;
var model_1 = require("../model/model");
function findAllCourses() {
    return model_1.CourseModel.findAll({
        order: ['seqNo']
    });
}
exports.findAllCourses = findAllCourses;
