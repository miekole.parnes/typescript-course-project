"use strict";
exports.__esModule = true;
exports.initRestApi = void 0;
var apiGetCourseDetail_1 = require("./apiGetCourseDetail");
var apiGetAllCourses_1 = require("./apiGetAllCourses");
var apiCreateLesson_1 = require("./apiCreateLesson");
function initRestApi(app) {
    app.route('/api/courses').get(apiGetAllCourses_1.apiGetAllCourses);
    app.route('/api/courses/:id').get(apiGetCourseDetail_1.apiGetCourseDetail);
    app.route('/api/lesson').post(apiCreateLesson_1.apiCreateLesson);
}
exports.initRestApi = initRestApi;
