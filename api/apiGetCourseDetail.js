"use strict";
exports.__esModule = true;
exports.apiGetCourseDetail = void 0;
var onSuccess_1 = require("./onSuccess");
var onError_1 = require("./onError");
var findCourseDetail_1 = require("../data/queries/findCourseDetail");
var _ = require("lodash");
function apiGetCourseDetail(req, res) {
    var courseId = parseInt(req.params.id);
    console.log(courseId);
    (0, findCourseDetail_1.findCourseDetail)(courseId)
        .then(_.partial(onSuccess_1.onSuccess, res))["catch"](_.partial(onError_1.onError, res, 'Could not find course detail for id'));
}
exports.apiGetCourseDetail = apiGetCourseDetail;
