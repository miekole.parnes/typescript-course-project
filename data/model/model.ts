const Sequelize = require('@sequelize/core');
import { initCourseModel } from './initCourseModel';
import { initLessonModel } from './initLessonModel';

const dbUrl =
  'postgres://postgres:postgres@localhost:5432/complete-typescript-course';

const sequelize = new Sequelize(dbUrl);

export const CourseModel = initCourseModel(sequelize);
export const LessonModel = initLessonModel(sequelize);

CourseModel.hasMany(LessonModel, { foreignKey: 'courseId' });
LessonModel.belongsTo(CourseModel, { foreignKey: 'courseId' });
