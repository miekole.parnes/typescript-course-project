"use strict";
exports.__esModule = true;
exports.findCourseDetail = void 0;
var model_1 = require("../model/model");
function findCourseDetail(courseId) {
    return model_1.CourseModel.findByPk(courseId, {
        include: [
            {
                model: model_1.LessonModel
            },
        ]
    });
}
exports.findCourseDetail = findCourseDetail;
