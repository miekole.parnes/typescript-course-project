"use strict";
exports.__esModule = true;
exports.onError = void 0;
function onError(res, message, err) {
    console.error('Promise chain error', message, err);
    res.status(500).send();
}
exports.onError = onError;
