import { CourseModel, LessonModel } from '../model/model';

export function findCourseDetail(courseId: number) {
  return CourseModel.findByPk(courseId, {
    include: [
      {
        model: LessonModel,
      },
    ],
  });
}
