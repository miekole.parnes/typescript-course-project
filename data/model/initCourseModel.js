"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
exports.initCourseModel = void 0;
var core_1 = require("@sequelize/core");
function initCourseModel(seq) {
    var CourseModel = /** @class */ (function (_super) {
        __extends(CourseModel, _super);
        function CourseModel() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return CourseModel;
    }(core_1.Model));
    return CourseModel.init({
        description: { type: core_1.DataTypes.STRING },
        url: { type: core_1.DataTypes.STRING },
        longDescription: { type: core_1.DataTypes.TEXT },
        iconUrl: { type: core_1.DataTypes.STRING },
        courseListIcon: { type: core_1.DataTypes.STRING },
        seqNo: { type: core_1.DataTypes.INTEGER },
        comingSoon: { type: core_1.DataTypes.BOOLEAN },
        isNew: { type: core_1.DataTypes.BOOLEAN },
        isOngoing: { type: core_1.DataTypes.BOOLEAN }
    }, {
        modelName: 'Courses',
        sequelize: seq
    });
}
exports.initCourseModel = initCourseModel;
