"use strict";
exports.__esModule = true;
exports.apiErrorHandler = void 0;
function apiErrorHandler(err, req, res, next) {
    console.log('API error handler', err);
    res
        .status(500)
        .json({ errorCode: 'ERR-001', message: 'Internal Server Error' });
}
exports.apiErrorHandler = apiErrorHandler;
