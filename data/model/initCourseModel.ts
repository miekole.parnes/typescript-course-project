import { Sequelize, Model, DataTypes } from '@sequelize/core';

export function initCourseModel(seq: Sequelize) {
  class CourseModel extends Model {}
  return CourseModel.init(
    {
      description: { type: DataTypes.STRING },
      url: { type: DataTypes.STRING },
      longDescription: { type: DataTypes.TEXT },
      iconUrl: { type: DataTypes.STRING },
      courseListIcon: { type: DataTypes.STRING },
      seqNo: { type: DataTypes.INTEGER },
      comingSoon: { type: DataTypes.BOOLEAN },
      isNew: { type: DataTypes.BOOLEAN },
      isOngoing: { type: DataTypes.BOOLEAN },
    },
    {
      modelName: 'Courses',
      sequelize: seq,
    }
  );
}
