"use strict";
exports.__esModule = true;
exports.onSuccess = void 0;
function onSuccess(res, data) {
    res.status(200).json({ payload: data });
}
exports.onSuccess = onSuccess;
